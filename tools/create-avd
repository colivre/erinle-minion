#!/usr/bin/env node

const fs = require('fs');
const pJoin = require('path').join;
const ANDROID_HOME = process.env.ANDROID_HOME;

const API_VERSIONS = fs.readdirSync(pJoin(ANDROID_HOME,'system-images'))
                       .filter((dir)=> dir.match(/^android-[0-9]+/i) );

const {run, runOpts, question} = require('./helpers');

console.log('Creating a new Android Virtual Device');

function humanAPIName(api) {
  return api[0].toUpperCase() + api.substr(1).replace(/-/g,' ');
}

console.log('\nAvaliable Android API versions:');
API_VERSIONS.forEach((version, i)=> {
  console.log(`   ${i+1}: ${humanAPIName(version)}`);
});

question(`   Select [1..${API_VERSIONS.length}] `, (opt) => {
  api = API_VERSIONS[parseInt(opt)-1];
  console.log(`You have selected "${humanAPIName(api)}".`);
  selectABI(api);
});

function selectABI(api) {
  var apiDir = pJoin(ANDROID_HOME,'system-images',api);
  const ABI_OPTIONS = run('find', apiDir, '-maxdepth', '2').split('\n')
                      .filter((line)=> line.match(/\/google_apis.*\/.+/i))
                      .map((line)=> line.replace(/^.*\/google_apis/,''));

  function humanABIName(abi) {
    return abi.replace(/^[^a-z0-9]/i,'').replace('/', ' ');
  }

  console.log('\nAvaliable Android ABI options:');
  ABI_OPTIONS.forEach((option, i)=> {
    console.log(`   ${i+1}: ${humanABIName(option)}`);
  });

  question(`   Select [1..${ABI_OPTIONS.length}] `, (opt) => {
    abi = ABI_OPTIONS[parseInt(opt)-1];
    console.log(`You have selected "${humanABIName(abi)}".`);
    createAVD(api, abi);
  });
}

function createAVD(api, abi) {
  question(`\n Give it a name: `, (name) => {
    console.log('\nBuilding...');
    var pack = `system-images;${api};google_apis${abi.replace('/',';')}`;
    var dir = pJoin(ANDROID_HOME, 'tools');
    var opts = {pwd: dir, stdio: 'inherit'};
    var tool = pJoin(dir,'bin','avdmanager');
    runOpts(opts, tool, 'create', 'avd', '--name', name, '--package', pack);
  });
}
