const readline = require('readline');
const spawnSync = require('child_process').spawnSync;

exports.run = function run(bin, ...args) {
  return exports.runOpts({}, bin, ...args);
};

exports.runOpts = function runOpts(opts, bin, ...args) {
  var resp = spawnSync(bin, args, opts);
  if (resp.status !== 0) throw((resp.stderr||`Error ${resp.status}`).toString());
  return (resp.stdout||'').toString();
};

exports.question = function question(txt, callback) {
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  rl.question(txt, (resp) => {
    rl.close();
    setTimeout(()=> callback(resp), 50);
  });
};
