'use strict';

var app = require('application');
var permissions = require('nativescript-permissions');

var utils = require('utils/utils');
const SmsManager = android.telephony.SmsManager
const sms = SmsManager.getDefault();
const PendingIntent = android.app.PendingIntent;

function testSmsResult(resultCode) {
  var result = { message: '', code: resultCode, ok: false };
  switch (resultCode) {
    case android.app.Activity.RESULT_OK:
      result.ok = true;
      result.message = 'SUCCESS'; break;
    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
      result.message = 'GENERIC FAILURE'; break;
    case SmsManager.RESULT_ERROR_RADIO_OFF:
      result.message = 'RADIO OFF'; break;
    case SmsManager.RESULT_ERROR_NULL_PDU:
      result.message = 'NULL PDU'; break;
    case SmsManager.RESULT_ERROR_NO_SERVICE:
      result.message = 'NO SERVICE'; break;
    default:
      result.message = 'Unkowun SMS intent result num('+result+').';
  }
  return result;
}

exports.send = function sendSMS(telNumber, message, onChangeStatus) {
  if (typeof onChangeStatus !== 'function') onChangeStatus = ()=>{};
  return new Promise((resolve, reject) => {
    var fail = (err)=> {
      onChangeStatus('fail', err);
      reject(err);
    };
    permissions.requestPermission(android.Manifest.permission.SEND_SMS, "Enviar SMS é a minha única função. :-)")
    .then(function() {
      try {
        onChangeStatus('sending');
        var wasSent = false;
        var failTimout = null;

        var context = utils.ad.getApplicationContext();
        var id = Math.random().toString().split('.')[1];
        var intentSendID = 'send-sms-'+id;
        var intentDeliverID = 'deliver-sms-'+id;
        var intentSend = new android.content.Intent(intentSendID);
        var intentDeliver = new android.content.Intent(intentDeliverID);
        var piSend = android.app.PendingIntent.getBroadcast(context, 0,
            intentSend, android.app.PendingIntent.FLAG_UPDATE_CURRENT);
        var piDeliver = android.app.PendingIntent.getBroadcast(context, 0,
            intentDeliver, android.app.PendingIntent.FLAG_UPDATE_CURRENT);

        sms.sendTextMessage(telNumber, null, message, piSend, piDeliver);

        app.android.registerBroadcastReceiver(intentSendID, function(context, intent) {
          var result = testSmsResult(this.getResultCode());
          if (result.ok) {
            wasSent = true;
            onChangeStatus('maybe-sent');
          }
          else fail(Error(result.message));
        });

        app.android.registerBroadcastReceiver(intentDeliverID, function(context, intent) {
          var result = testSmsResult(this.getResultCode());
          if (result.ok) {
            clearTimeout(failTimout);
            onChangeStatus('delivered');
            resolve();
          }
          else fail(Error(result.message));
        });

        var failTimout = setTimeout(()=>{
          if (wasSent) {
            onChangeStatus('sent');
            resolve();
          } else {
            fail(Error(`SMS Send Timeout. Can't deliver.`));
          }
        }, 5*60*1000); // 5 minutes.

      } catch(err) {
        fail(Error('SMS Send code crash: '+err.message));
      }
    })
    .catch(function(err) {
       fail(Error('Request permission "SEND_SMS" Fail: '+err.message));
       alert('Não posso fazer o meu trabalho sem a permissão de envio de SMS. :-(');
    });
  });
}
