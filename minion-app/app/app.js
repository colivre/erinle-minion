'use strict';

var app = require('application');
var mainPage = require('./main-page');
var httpCli = require('fetch');
var sms = require('./sms');
var Label = require("ui/label").Label;
var { ObservableArray } = require('tns-core-modules/data/observable-array');
//var player, TNSPlayer = require('nativescript-audio').TNSPlayer;

app.mainWorkActive = false;

var logMemo = new ObservableArray([]);

function removeLog(id) {
  var i, len=logMemo.length;
  for (i=0; i<len; i++) {
    if (logMemo.getItem(i).id === id) {
      logMemo.splice(i, 1);
      break;
    }
  }
}

function log(msg, type) {
  try {
    var id = Date.now() +':'+ parseInt(Math.random() * 1000);
    type = type || 'info';
    console.log('MINION '+type+':', msg);
    if (msg.stack) console.error(msg.stack);
    //msg = (new Date()) +'\n'+ msg;
    logMemo.unshift({ id, msg, type, time: new Date() });
    mainPage.logList.items = logMemo;
    setTimeout(()=> removeLog(id), type==='error' ? 1000*60*60*4 : 1000*60);
  } catch(err) {
    console.error('MINION LOG FAIL!');
    console.error(err);
    alert(err.message);
  }
}

log.error = (err)=> {
  /*try {
    player.playFromFile({audioFile: '~/audio/doh.wav', loop: false}) //Random Volume Error
    .catch((err)=> console.log('Fail to play sound. '+ err.message) );
  } catch(err){
    console.log('Fail to play sound. '+ err.message);
  }*/
  if (!err.message) err = new Error(err);
  log(err.message, 'error');
}

log.debug = (...msg)=> {
  log(msg.join('\t'), 'debug');
}

function errCtx(msg, err) {
  return {
    message: msg +'\t'+ err.message,
    stack: err.stack
  }
}

function apiURL(path, params) {
  if (!path && !path.push) throw Error('path must be an array');
  if (!params) params = {};
  var url = mainPage.serverUrlField.text + '/api/v1';
  for (var i=0; i<path.length; i++) url += '/'+ path[i];
  url += '.json?key=' + mainPage.getMinionKey();
  for (var key in params) url += '&'+ key +'='+ escape(params[key]);
  return url;
}

function updateMsgStatus(message, status, error) {
  var params = {status: status}
  if (error) params.error = error;
  var url = apiURL(['message', message.id], params);
  log.debug(`Update Status ${url}`)
  return httpCli.fetch(url, {
    method: 'GET',
    headers: { 'User-Agent': 'Erinle Minion' }
  })
}

function removeTelLabel(label) {
  //log.debug(`Removendo tel label ${label ? label.text : 'indefinido'}`)
  if (label && label.parentNode) {
    mainPage.sendStatusBox.removeChild(label)
  }
}

function sendMessages(messages) {
  log.debug(`Sending ${messages.length} messages.`)
  messages.forEach((message)=> {
    var phone = message.phone.replace(/[^0-9]/g, '');
    log.debug(`Send SMS for ${phone}: ${message.text}`);
    var telLabel = new Label();
    telLabel.text = phone;
    mainPage.sendStatusBox.addChild(telLabel);
    sms.send(phone, message.text, (stat)=> {
      log.debug('stat '+phone, stat);
      telLabel.className = 'tel-label-'+stat;
    })
    .then((msg)=> {
      log.debug(`${phone} Deliver Success!`);
      setTimeout(()=> removeTelLabel(telLabel), 10*1000);
      updateMsgStatus(message, 'completed')
      .then((response)=> log.debug(`${phone} Deliver Success recorded!`) )
      .catch((err)=> log.error(errCtx(`${phone} Deliver Success NOT recorded!`, err)) )
    })
    .catch((err)=> {
      log.error(errCtx(phone+' FAIL:',err));
      console.error(err);
      setTimeout(()=> removeTelLabel(telLabel), 30*1000);
      updateMsgStatus(message, 'failed', err.message)
      .then((response)=> log.debug(`${phone} Deliver Fail recorded!`) )
      .catch((err)=> log.error(errCtx(`${phone} Deliver Fail NOT recorded!`, err)) )
    });
  });
}

function requestMessages() {
  try {
    if (app.mainWorkActive) {
      var url = apiURL(['message']);
      //log.debug('Request updates: '+url);

      httpCli.fetch(url, {
        method: 'GET',
        headers: { 'User-Agent': 'Erinle Minion' }
      })
      .then(function(response) {
        if (!response.ok) throw Error(`Falha na requisição HTTP: (Erro ${response.status}) ${response.statusText}`);
        try {
          return response.json();
        } catch(err) {
          log.debug(`response.json() fail: ${err.message}\nfor:\n${response.text()}`);
          throw Error(`Erro na estrutura de dados enviada pelo servidor: ${err.message}`);
        }
      })
      .then(function(response) {
        if (response.ok) sendMessages(response.messages);
        else throw Error('A resposta do servidor contém erro: '+ response.error);
      })
      .catch(function(error) {
        log.error(error);
      });
    }
  } catch(err) {
    log.error(err);
  }
  setTimeout(requestMessages, (app.mainWorkActive ? 30 * 1000 : 800));
}

app.on(app.launchEvent, function (args) {
  // args.android is an android.content.Intent class.
  console.log('Launched Minion App -- ' + args.android);
});

app.on(app.displayedEvent, function () {
  console.log('App Displayed!');
  setTimeout(()=> { // Ensure mainPage's pageLoaded run.
    mainPage.toggleActive();
    //player = new TNSPlayer();
    requestMessages();
  }, 500);
});

app.on(app.suspendEvent, function (args) {
  // args.android is an android activity class.
  console.log('suspendEvent: ' + args.android);
});

app.on(app.resumeEvent, function (args) {
  // args.android is an android activity class.
  console.log('resumeEvent: ' + args.android);
});

app.on(app.exitEvent, function (args) {
  // args.android is an android activity class.
  console.log('exitEvent: ' + args.android);
});

app.on(app.lowMemoryEvent, function (args) {
  // args.android is an android activity class.
  console.log('lowMemoryEvent: ' + args.android);
});

app.on(app.uncaughtErrorEvent, function (args) {
  var err = args.android; // args.android is an NativeScriptError.
  console.error(
    '\n-------------------------------------------------------------------\n',
    'Uncaught NativeScriptError: '+ err.message,
    '\n-------------------------------------------------------------------\n',
    err.stackTrace,
    '\n-------------------------------------------------------------------\n'
  );
  alert('Ups... Erro não tratado!\n' + err.message);
  return false
});

app.start({ moduleName: 'main-page' });
