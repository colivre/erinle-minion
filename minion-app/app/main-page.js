'use strict';

var app = require('application');
var settings = require("application-settings");
var permissions = require('nativescript-permissions');

var cacheMinionKey = null;
exports.getMinionKey = function getMinionKey() {
  if (!cacheMinionKey) {
    cacheMinionKey = settings.getString('minion-key');
    if (!cacheMinionKey) {
      cacheMinionKey = Math.random().toString(16).toUpperCase().split('.')[1];
      settings.setString('minion-key', cacheMinionKey);
    }
  }
  return cacheMinionKey;
}
exports.toggleActive = function toggleActive() {
  var field = exports.serverUrlField;
  app.mainWorkActive = !app.mainWorkActive;
  if (!field.text) {
    app.mainWorkActive = false;
    return alert('Não posso trabalhar sem um servidor Erinle!');
  }

  if (app.mainWorkActive) {
    field.text = field.text.replace(/\s/g,'');
    if (field.text.replace(/https?:\/*/,'') === '') {
      // If undefined URL do not start!
      app.mainWorkActive = false;
      return alert('Não posso trabalhar sem um servidor Erinle!');
    }
    // Prefix the protocol, if missed:
    if (!/^[a-z]+:\/+/i.test(field.text)) field.text = 'http://' + field.text;
    // Downcase the domain:
    field.text = field.text.replace(/^[a-z]+:\/+[.a-z]+/i, (str)=> str.toLowerCase());
    // Save it:
    settings.setString('server-url', field.text);
    exports.btRun.text = 'Parar';
    field.isEnabled = false;
  } else {
    exports.btRun.text = 'Iniciar';
    field.isEnabled = true;
  }
}

exports.pageLoaded = function(args) {
  console.log('Main Page Loaded.');
  permissions.requestPermission(android.Manifest.permission.SEND_SMS, "Enviar SMS é a minha única função. :-)")

  var page = args.object;
  page.getViewById('key').text = exports.getMinionKey();

  // prevent the soft keyboard from showing initially when textfields are present
  app.android.startActivity.getWindow().setSoftInputMode(
    android.view.WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN
  );

  exports.btRun = page.getViewById('run');
  exports.serverUrlField = page.getViewById('server-url');
  exports.sendStatusBox = page.getViewById('send-status');
  exports.logList = page.getViewById('log');

  if (!exports.serverUrlField.text) exports.serverUrlField.text = settings.getString('server-url');
};
