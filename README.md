Erinle Minion
=============

An Android App for sending SMS for Erinle distributed application.

Dev Install
-----------

### Step 1 — Install Android SDK

Select the install source, Debian package or Android.com:

#### Step 1.1 — Install SDK from Debian package
```bash
sudo aptitude install android-sdk android-sdk-platform-23 google-android-build-tools-23-installer
# optional sudo aptitude install google-android-platform-23-installer
export ANDROID_HOME=/usr/lib/android-sdk/ # set this in your .bashrc
```

#### Step 1.2 — Install SDK from Android.com
* Download the installer from https://developer.android.com/studio
* Follow the site instructions to install.
* than:
```bash
export ANDROID_HOME=/where/you/install/android-sdk/ # set this in your .bashrc
touch ~/.android/repositories.cfg
$ANDROID_HOME/tools/bin/sdkmanager "tools" "platform-tools" \
  "platforms;android-25" "build-tools;25.0.2" \
  "extras;android;m2repository" "extras;google;m2repository"
```

### Step 2 — Clone and install dependencies from NPM
```bash
git clone git@gitlab.com:colivre/erinle-minion.git
cd erinle-minion
npm install
```

### Step 3 — Install NativeScript build files for Android
```bash
npm run tns platform add android
```

### Step 4 — Create an Android Virtual Device
```bash
npm run create-avd
```

### Step 5 — Launch a Device Emulator
```bash
npm run launch-avd
```

### Step 6 — Build and run the App
```bash
npm start
```

#### Optional: Build and run the App in a specific device
```bash
npm start -- --device <device-id>
```

